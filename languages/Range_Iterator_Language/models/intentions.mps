<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:a99cc2a5-a075-45fa-ad6c-64ccbc3346d0(Range_Iterator_Language.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="0" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="j9x2" ref="r:5bfdaaaa-1851-491c-9542-d3b5df4428f5(Range_Iterator_Language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431790191" name="jetbrains.mps.baseLanguage.structure.Expression" flags="nn" index="33vP2n" />
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795771125" name="jetbrains.mps.lang.intentions.structure.IsApplicableBlock" flags="in" index="2SaL7w" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093995" name="isApplicableFunction" index="2ZfVeh" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="1196350785110" name="jetbrains.mps.lang.quotation.structure.AbstractAntiquotation" flags="ng" index="2c44t0">
        <child id="1196350785111" name="expression" index="2c44t1" />
      </concept>
      <concept id="1196350785112" name="jetbrains.mps.lang.quotation.structure.Antiquotation" flags="ng" index="2c44te" />
      <concept id="1196350785113" name="jetbrains.mps.lang.quotation.structure.Quotation" flags="nn" index="2c44tf">
        <child id="1196350785114" name="quotedNode" index="2c44tc" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2S6QgY" id="7YrrZ9MDjj4">
    <property role="TrG5h" value="DoubleIterations" />
    <ref role="2ZfgGC" to="j9x2:7YrrZ9MCzZu" resolve="RangeIterator" />
    <node concept="2S6ZIM" id="7YrrZ9MDjj5" role="2ZfVej">
      <node concept="3clFbS" id="7YrrZ9MDjj6" role="2VODD2">
        <node concept="3clFbF" id="7YrrZ9MDjsZ" role="3cqZAp">
          <node concept="Xl_RD" id="7YrrZ9MDjsY" role="3clFbG">
            <property role="Xl_RC" value="Double Iterations" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="7YrrZ9MDjj7" role="2ZfgGD">
      <node concept="3clFbS" id="7YrrZ9MDjj8" role="2VODD2">
        <node concept="3SKdUt" id="7YrrZ9MDLUB" role="3cqZAp">
          <node concept="3SKdUq" id="7YrrZ9MDLUD" role="3SKWNk">
            <property role="3SKdUp" value="MulExpression + Antiquotations" />
          </node>
        </node>
        <node concept="3clFbF" id="7YrrZ9MDBPd" role="3cqZAp">
          <node concept="37vLTI" id="7YrrZ9MDCMi" role="3clFbG">
            <node concept="2OqwBi" id="7YrrZ9MDBYR" role="37vLTJ">
              <node concept="2Sf5sV" id="7YrrZ9MDBPb" role="2Oq$k0" />
              <node concept="3TrEf2" id="7YrrZ9MDCuV" role="2OqNvi">
                <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAK8" resolve="upper_boundary" />
              </node>
            </node>
            <node concept="2c44tf" id="7YrrZ9MDI_6" role="37vLTx">
              <node concept="17qRlL" id="7YrrZ9MDKMI" role="2c44tc">
                <node concept="3cmrfG" id="7YrrZ9MDKQI" role="3uHU7B">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="1eOMI4" id="7YrrZ9MDKZ5" role="3uHU7w">
                  <node concept="33vP2n" id="7YrrZ9MDKZ6" role="1eOMHV">
                    <node concept="2c44te" id="7YrrZ9MDL4S" role="lGtFl">
                      <node concept="2OqwBi" id="7YrrZ9MDLiX" role="2c44t1">
                        <node concept="2Sf5sV" id="7YrrZ9MDL98" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7YrrZ9MDLN1" role="2OqNvi">
                          <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAK8" resolve="upper_boundary" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7YrrZ9MDI9U" role="3cqZAp" />
      </node>
    </node>
    <node concept="2SaL7w" id="7YrrZ9MDkDt" role="2ZfVeh">
      <node concept="3clFbS" id="7YrrZ9MDkDu" role="2VODD2">
        <node concept="3clFbF" id="7YrrZ9MDkKK" role="3cqZAp">
          <node concept="2OqwBi" id="7YrrZ9MDm8H" role="3clFbG">
            <node concept="2OqwBi" id="7YrrZ9MDl05" role="2Oq$k0">
              <node concept="2Sf5sV" id="7YrrZ9MDkKJ" role="2Oq$k0" />
              <node concept="3TrEf2" id="7YrrZ9MDlAW" role="2OqNvi">
                <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAK8" resolve="upper_boundary" />
              </node>
            </node>
            <node concept="3x8VRR" id="7YrrZ9MDmC2" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

