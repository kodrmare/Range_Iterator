<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ecaa1f7e-2a5c-40dc-970f-34921a002060(Range_Iterator_Language.dataFlow)">
  <persistence version="9" />
  <languages>
    <use id="7fa12e9c-b949-4976-b4fa-19accbc320b4" name="jetbrains.mps.lang.dataFlow" version="1" />
    <devkit ref="00000000-0000-4000-0000-443879f56b80(jetbrains.mps.devkit.aspect.dataflow)" />
  </languages>
  <imports>
    <import index="j9x2" ref="r:5bfdaaaa-1851-491c-9542-d3b5df4428f5(Range_Iterator_Language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="7fa12e9c-b949-4976-b4fa-19accbc320b4" name="jetbrains.mps.lang.dataFlow">
      <concept id="1207062474157" name="jetbrains.mps.lang.dataFlow.structure.EmitLabelStatement" flags="ng" index="axUMO" />
      <concept id="1207062697254" name="jetbrains.mps.lang.dataFlow.structure.LabelPosition" flags="ng" index="ayLgZ">
        <reference id="1207062703832" name="label" index="ayMZ1" />
      </concept>
      <concept id="1206442055221" name="jetbrains.mps.lang.dataFlow.structure.DataFlowBuilderDeclaration" flags="ig" index="3_zdsH">
        <reference id="1206442096288" name="conceptDeclaration" index="3_znuS" />
        <child id="1206442812839" name="builderBlock" index="3_A6iZ" />
      </concept>
      <concept id="1206442659665" name="jetbrains.mps.lang.dataFlow.structure.BuilderBlock" flags="in" index="3__wT9" />
      <concept id="1206442747519" name="jetbrains.mps.lang.dataFlow.structure.NodeParameter" flags="nn" index="3__QtB" />
      <concept id="1206444910183" name="jetbrains.mps.lang.dataFlow.structure.RelativePosition" flags="ng" index="3_I6tZ">
        <child id="1206444923842" name="relativeTo" index="3_I9Fq" />
      </concept>
      <concept id="1206445082906" name="jetbrains.mps.lang.dataFlow.structure.AfterPosition" flags="ng" index="3_IKw2" />
      <concept id="1206445181593" name="jetbrains.mps.lang.dataFlow.structure.BaseEmitJumpStatement" flags="nn" index="3_J8I1">
        <child id="1206445193860" name="jumpTo" index="3_JbIs" />
      </concept>
      <concept id="1206445295557" name="jetbrains.mps.lang.dataFlow.structure.EmitIfJumpStatement" flags="nn" index="3_J$rt" />
      <concept id="1206445310309" name="jetbrains.mps.lang.dataFlow.structure.EmitJumpStatement" flags="nn" index="3_JC1X" />
      <concept id="1206454052847" name="jetbrains.mps.lang.dataFlow.structure.EmitCodeForStatement" flags="nn" index="3AgYrR">
        <child id="1206454079161" name="codeFor" index="3Ah4Yx" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3_zdsH" id="7YrrZ9MDdGL">
    <ref role="3_znuS" to="j9x2:7YrrZ9MCzZu" resolve="RangeIterator" />
    <node concept="3__wT9" id="7YrrZ9MDdGM" role="3_A6iZ">
      <node concept="3clFbS" id="7YrrZ9MDdGN" role="2VODD2">
        <node concept="3AgYrR" id="7YrrZ9MDdH6" role="3cqZAp">
          <node concept="2OqwBi" id="7YrrZ9MDdRh" role="3Ah4Yx">
            <node concept="3__QtB" id="7YrrZ9MDdHv" role="2Oq$k0" />
            <node concept="3TrEf2" id="7YrrZ9MDemd" role="2OqNvi">
              <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAK8" resolve="upper_boundary" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7YrrZ9MEeHj" role="3cqZAp" />
        <node concept="axUMO" id="7YrrZ9MDerP" role="3cqZAp">
          <property role="TrG5h" value="startIteration" />
        </node>
        <node concept="3_J$rt" id="7YrrZ9MDeyj" role="3cqZAp">
          <node concept="3_IKw2" id="7YrrZ9MDe_E" role="3_JbIs">
            <node concept="3__QtB" id="7YrrZ9MDeA5" role="3_I9Fq" />
          </node>
        </node>
        <node concept="3AgYrR" id="7YrrZ9MDeDB" role="3cqZAp">
          <node concept="2OqwBi" id="7YrrZ9MDeQ_" role="3Ah4Yx">
            <node concept="3__QtB" id="7YrrZ9MDeGN" role="2Oq$k0" />
            <node concept="3TrEf2" id="7YrrZ9MDflx" role="2OqNvi">
              <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAKa" resolve="body" />
            </node>
          </node>
        </node>
        <node concept="3_JC1X" id="7YrrZ9MDfrL" role="3cqZAp">
          <node concept="ayLgZ" id="7YrrZ9MDfvV" role="3_JbIs">
            <ref role="ayMZ1" node="7YrrZ9MDerP" resolve="startIteration" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

