<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:37c15845-65d4-4dcd-bc83-268f4a21ee35(Range_Iterator_Language.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="j9x2" ref="r:5bfdaaaa-1851-491c-9542-d3b5df4428f5(Range_Iterator_Language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="7YrrZ9MCEUo">
    <ref role="1XX52x" to="j9x2:7YrrZ9MCzZu" resolve="RangeIterator" />
    <node concept="3EZMnI" id="7YrrZ9MCEUq" role="2wV5jI">
      <node concept="3F0ifn" id="7YrrZ9MEfWc" role="3EZMnx">
        <property role="3F0ifm" value="from" />
      </node>
      <node concept="3F1sOY" id="7YrrZ9MEfX5" role="3EZMnx">
        <ref role="1NtTu8" to="j9x2:7YrrZ9MDOR9" resolve="lower_boundary" />
      </node>
      <node concept="3F0ifn" id="7YrrZ9MEfY2" role="3EZMnx">
        <property role="3F0ifm" value="to" />
      </node>
      <node concept="3F1sOY" id="7YrrZ9MCEUx" role="3EZMnx">
        <ref role="1NtTu8" to="j9x2:7YrrZ9MCAK8" resolve="upper_boundary" />
      </node>
      <node concept="3F0ifn" id="7YrrZ9MEhMS" role="3EZMnx">
        <property role="3F0ifm" value="iterate" />
      </node>
      <node concept="3F0ifn" id="7YrrZ9MCEUJ" role="3EZMnx">
        <property role="3F0ifm" value="{" />
      </node>
      <node concept="3F1sOY" id="7YrrZ9MCEUZ" role="3EZMnx">
        <ref role="1NtTu8" to="j9x2:7YrrZ9MCAKa" resolve="body" />
        <node concept="pVoyu" id="7YrrZ9MCEVk" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7YrrZ9MCEVo" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7YrrZ9MCEVb" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="pVoyu" id="7YrrZ9MCEVm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="7YrrZ9MCEUt" role="2iSdaV" />
    </node>
  </node>
</model>

