<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f245740a-7c1e-44bf-ae6d-2ff8d9338011(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="j9x2" ref="r:5bfdaaaa-1851-491c-9542-d3b5df4428f5(Range_Iterator_Language.structure)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1168559333462" name="jetbrains.mps.lang.generator.structure.TemplateDeclarationReference" flags="ln" index="j$656" />
      <concept id="1095672379244" name="jetbrains.mps.lang.generator.structure.TemplateFragment" flags="ng" index="raruj" />
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1092059087312" name="jetbrains.mps.lang.generator.structure.TemplateDeclaration" flags="ig" index="13MO4I">
        <reference id="1168285871518" name="applicableConcept" index="3gUMe" />
        <child id="1092060348987" name="contentNode" index="13RCb5" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="7YrrZ9MCqKR">
    <property role="TrG5h" value="main" />
    <node concept="3aamgX" id="7YrrZ9MCW_h" role="3acgRq">
      <ref role="30HIoZ" to="j9x2:7YrrZ9MCzZu" resolve="RangeIterator" />
      <node concept="j$656" id="7YrrZ9MCW_n" role="1lVwrX">
        <ref role="v9R2y" node="7YrrZ9MCW_l" resolve="reduce_RangeIterator" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="7YrrZ9MCW_l">
    <property role="TrG5h" value="reduce_RangeIterator" />
    <ref role="3gUMe" to="j9x2:7YrrZ9MCzZu" resolve="RangeIterator" />
    <node concept="1Dw8fO" id="7YrrZ9MCW_q" role="13RCb5">
      <node concept="raruj" id="7YrrZ9MCW_u" role="lGtFl" />
      <node concept="3cpWsn" id="7YrrZ9MCW_y" role="1Duv9x">
        <property role="TrG5h" value="i" />
        <node concept="10Oyi0" id="7YrrZ9MCW_E" role="1tU5fm" />
        <node concept="3cmrfG" id="7YrrZ9MCW_U" role="33vP2m">
          <property role="3cmrfH" value="0" />
        </node>
      </node>
      <node concept="3eOVzh" id="7YrrZ9MCXoq" role="1Dwp0S">
        <node concept="3cmrfG" id="7YrrZ9MCXoF" role="3uHU7w">
          <property role="3cmrfH" value="10" />
          <node concept="29HgVG" id="7YrrZ9MD0UR" role="lGtFl">
            <node concept="3NFfHV" id="7YrrZ9MD0US" role="3NFExx">
              <node concept="3clFbS" id="7YrrZ9MD0UT" role="2VODD2">
                <node concept="3clFbF" id="7YrrZ9MD0UZ" role="3cqZAp">
                  <node concept="2OqwBi" id="7YrrZ9MD0UU" role="3clFbG">
                    <node concept="3TrEf2" id="7YrrZ9MD3Qc" role="2OqNvi">
                      <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAK8" resolve="numberOfIterations" />
                    </node>
                    <node concept="30H73N" id="7YrrZ9MD0UY" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="37vLTw" id="7YrrZ9MCWA3" role="3uHU7B">
          <ref role="3cqZAo" node="7YrrZ9MCW_y" resolve="i" />
        </node>
      </node>
      <node concept="3uNrnE" id="7YrrZ9MCZC_" role="1Dwrff">
        <node concept="37vLTw" id="7YrrZ9MCZCB" role="2$L3a6">
          <ref role="3cqZAo" node="7YrrZ9MCW_y" resolve="i" />
        </node>
      </node>
      <node concept="3clFbS" id="7YrrZ9MCZZC" role="2LFqv$">
        <node concept="3clFbF" id="7YrrZ9MD04g" role="3cqZAp">
          <node concept="2OqwBi" id="7YrrZ9MD04d" role="3clFbG">
            <node concept="10M0yZ" id="7YrrZ9MD04e" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="7YrrZ9MD04f" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(int):void" resolve="println" />
              <node concept="37vLTw" id="7YrrZ9MD05K" role="37wK5m">
                <ref role="3cqZAo" node="7YrrZ9MCW_y" resolve="i" />
              </node>
            </node>
          </node>
          <node concept="29HgVG" id="7YrrZ9MD0u6" role="lGtFl">
            <node concept="3NFfHV" id="7YrrZ9MD0u7" role="3NFExx">
              <node concept="3clFbS" id="7YrrZ9MD0u8" role="2VODD2">
                <node concept="3clFbF" id="7YrrZ9MD0ue" role="3cqZAp">
                  <node concept="2OqwBi" id="7YrrZ9MD0u9" role="3clFbG">
                    <node concept="3TrEf2" id="7YrrZ9MD0uc" role="2OqNvi">
                      <ref role="3Tt5mk" to="j9x2:7YrrZ9MCAKa" resolve="body" />
                    </node>
                    <node concept="30H73N" id="7YrrZ9MD0ud" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

